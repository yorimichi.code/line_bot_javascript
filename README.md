# 使い方
1. .envファイルを作成し、LINEのチャンネルアクセストークンとシークレットキーを入力する。
```
ACCESS_TOKEN={チャンネルアクセストークン}
SECRET_KEY={シークレットキー}
```
2. モジュールインストール
```
yarn
```
3. 実行
```
node index.js
```
4. https://{domain}/webhook をLINE DevelopersのWebhook URLに設定。